#-*- encoding: utf-8 -*-
import datetime
import bs4 
import json

from django.shortcuts import render
from django.http import HttpResponseRedirect,HttpResponse
from django.core.urlresolvers import reverse
from django.views.generic import DetailView,ListView,TemplateView
from django.contrib.humanize.templatetags.humanize import naturaltime,naturalday

from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from models import *



class ActionsViews(ListView):
    def get_queryset(self):
        return Action.objects.filter(hide=False,man=self.request.user.man)
        
    def link1(self,act):
        if act.group==None:
            return act.link
        return act.group.get_absolute_url()
        
    def get(self,request,*args,**kwargs):
        qs = self.get_queryset()
        if request.GET.get("all","")=="":
            qs = qs.filter(is_read=False)
        if qs.count()==0: return HttpResponse(json.dumps([]))
        qs.update(is_read=True) 
        qs = qs[:50]       
        tid = 0
        
        return HttpResponse(json.dumps([{
            "id": d.id,
            "image": d.image_link,
            "title": d.title,
            "text":d.text,
            "link1":self.link1(d),
            "link2":d.link,
            "dt": naturalday(d.dt),
            "tid": d.topic_id,
            "dt_str": d.dt.strftime("%d.%m.%Y %H:%I"),
          }
          for d in qs
        ]))

class MessagesViews(ListView):
    def get_queryset(self):
        return Message.objects.filter(is_viewed=False,to_user=self.request.user)
    
    def get(self,request,*args,**kwargs):
        return HttpResponse(json.dumps({'count':self.get_queryset().count()/2}))
    

def importGroupTopics(request):
    soup = bs4.BeautifulSoup(request.read())
    
    items = soup.find("items")
    if items==None:
        return HttpResponse('500')
    try:
        gid = items['group_id']
        uid = items['user_id']
        pwd = items['password']
    except:
        return HttpResponse('500')
    
    man = Man.objects.get(user_id=uid)
    
    
    user = man.user#authenticate(username=man.user.username,password=pwd)
    #if user==None or not user.is_active:
    #    return HttpResponse('402')        
    
    group = Group.objects.get(pk=gid)
    
    #if ManGroup.objects.filter(man=man,group_id=gid,role=1).count()==0:
    #    return HttpResponse('403')
    
    
    for item in soup.find_all("item"):
        if GroupTopic.objects.filter(group=group,title__iexact=item.find("title").getText(),text__iexact=item.find("text").getText()).count()>0: 
            print "skipping",item.find("title").getText()
            continue
        print "adding",item.find("title").getText()
        topic = GroupTopic(group=group,author=man)
        topic.title = item.find("title").getText()[:1024]
        topic.text  = item.find("text").getText()
        if item.find("link")!=None:  topic.link  = item.find("link").getText()
        if item.find("extra")!=None: topic.extra = item.find("extra").getText()        
        topic.save()
        
        #topic.makeAction()
        #print "title=",item.find("title").getText()
        #print "link=",item.find("link").getText()
    return HttpResponse('200')
    
class GetIPView(TemplateView):
    def get(self,request,*args,**kwargs):
        return HttpResponse(request.META.get('REMOTE_ADDR'))
    
    
    
    
    