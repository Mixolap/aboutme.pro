#-*- encoding: utf-8 -*-
import os
import datetime
import random

from django.conf import settings
from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.db.models import Max
# 31.12.2014
def parseDate(dt):
    if dt==None: return
    date = dt.strip().split(".")
    if len(date)!=3: return
    
    return datetime.datetime(int(date[2]),int(date[1]),int(date[0]))
def yearName(y):    
    y = str(y)
    if len(y)==0: return ""
    y = int(y)
    if y>=5 and y<=20: return u"лет"
    y = str(y)
    if int(y[-1])==1: return u"год"
    if int(y[-1])>=2 and int(y[-1])<=4: return u"года"
    return u"лет"
    
## замена проблемных символов на латиницу
def ecran(s):
    s1 = [u"1",u"2",u"3",u"4",u"5",u"6",u"7",u"8",u"9",u"0",u"«",u"»",u"/",u"—",u'"',u"'",u']',u'[',u'?',u'+',u'•',u'#',u'–',u'а',u'б',u'в',u'г',u'д',u'е',u'ё',u'ж',u'з',u'и',u'й',u'к',u'л',u'м',u'н',u'о',u'п',u'р',u'с',u'т',u'у',u'ф',u'х',u'ц',u'ч',u'ш',u'щ',u'ъ',u'ы',u'ь',u'э',u'ю',u'я']
    s2 = [u"1",u"2",u"3",u"4",u"5",u"6",u"7",u"8",u"9",u"0",u" ",u" ",u" ",u" ",u" ",u" ",u' ',u' ',u' ',u' ',u'-',u'_','-','a','b','v','g','d','e','yo','j','z','i','i','k','l','m','n','o','p','r','s','t','u','f','x','ts','ch','sh','sh','','y','','e','y','ia','h','c','w','q']
    for i in xrange(len(s1)):        
        s = s.replace(s1[i],s2[i])
        s = s.replace(s1[i].upper(),s2[i].upper())      
    for i in xrange(len(s)):
        if s[i] in [".","-"]: continue        
        if s[i].lower() not in s2: 
            s=s.replace(s[i],"_")            
            continue
        if not (s[i].isalpha() or s[i].isnumeric()): s=s.replace(s[i],"_")
    return s

def genPasswordActivationCode():
    code = ""
    for i in xrange(10):
        d = random.randint(0,9)
        code += str(d)
        
    while ResetPassword.objects.filter(uid=code).count()>0:
        return genPasswordActivationCode()
    return code

def genPassword():
    code = ""
    for i in xrange(4):
        d = random.randint(0,9)
        code += str(d)
    return code

class ResetPassword(models.Model):
    email = models.CharField(max_length=255)
    uid   = models.CharField(max_length=255)
    dt    = models.DateTimeField(auto_now_add=True)


class Prof(models.Model):
    name = models.CharField(max_length=255)
    def __unicode__(self):
        return self.name

class Man(models.Model):
    user  = models.OneToOneField(User)
    name  = models.CharField(max_length=255)
    prof  = models.ForeignKey(Prof,null=True,blank=True)
    phone = models.CharField(max_length=255,null=True,blank=True)
    city  = models.CharField(max_length=255,null=True,blank=True,verbose_name=u"Город проживания")
    skype = models.CharField(max_length=255,null=True,blank=True)
    icq   = models.CharField(max_length=255,null=True,blank=True)
    image = models.ImageField(upload_to='static/image/man/%Y/%m/',null=True,blank=True,verbose_name=u"Изображение")
    interes = models.TextField(null=True,blank=True,verbose_name=u"Чем интересуется")
    is_active = models.BooleanField(default=False)
    activation_uid = models.CharField(max_length=255,null=True,blank=True,verbose_name=u"Ключ активации")
    slug  = models.SlugField(null=True,blank=True)
    aboutme = models.TextField(null=True,blank=True,verbose_name=u"Обо мне")
    def __unicode__(self):
        return self.name+" - "+self.user.username
    
    def getNewSlug(self):
        name = self.name[:50]
        tlink = ecran(name).replace(".","_").replace(" ","_").replace("-","").replace("__","_").strip("_")      
        if Man.objects.filter(slug=tlink).count()>0: tlink=str(Man.objects.all().order_by("-id")[0].id+2)+"_"+tlink
        slug = tlink.lower()[:50]
        if Man.objects.filter(slug=slug).count()>0: slug = str(Man.objects.aggregate(Max('id'))['id__max']+1)+'_'+slug
        return slug
        
    def save(self):
        if self.slug==None or self.slug=="": self.slug = self.getNewSlug()
        models.Model.save(self)
    
    def get_absolute_url(self):
        if self.slug==None or self.slug=='': self.save()
        return reverse('man',args=[self.slug])
        
    def naviks(self):
        return ManNavik.objects.filter(man=self).order_by("srok")
    
    def naviks_str(self):
        return ", ".join([d.navik.name for d in self.naviks()])
    
    def fullIntereses(self):
        if self.interes==None: return self.naviks_str()
        return self.naviks_str()+","+self.interes
        
    def projects(self):
        data = [d.project for d in ManProject.objects.filter(man=self)]
        data += list(Project.objects.filter(man=self).order_by("-id"))
        data.sort(lambda x,y: cmp(y.pk,x.pk))
        return data
    
    def projects_on_main(self):
        data = [d.project for d in ManProject.objects.filter(man=self,project__on_main=True)]
        data += list(Project.objects.filter(man=self,on_main=True).order_by("-id"))
        data.sort(lambda x,y: cmp(y.pk,x.pk))
        return data
        
    def groups(self):
        return [d.group for d in ManGroup.objects.filter(man=self)]
    
    def topics(self):
        return [d.topic for d in ManTopicBookmark.objects.filter(man=self,topic__is_active=True).order_by("-id")]

    def my_topics(self):
        return [d.topic for d in ManTopicBookmark.objects.filter(man=self).order_by("-id")]

    def last_topics(self):
        return [d.group.last_topic() for d in ManGroup.objects.filter(man=self)]
    
    def actions(self):
        return Action.objects.filter(man=self,hide=False).order_by("-id")
    
    def get_image_url(self):
        if self.image==None or self.image=="": return "/static/images/default_avatar.jpg"
        return "/"+self.image.url
        
    def podpis1(self):
        return ManPodpis.objects.filter(man1=self)

    def podpis2(self):
        return ManPodpis.objects.filter(man2=self)

class Project(models.Model):    
    man   = models.ForeignKey(Man,null=True,blank=True)
    link  = models.CharField(max_length=255,null=True,blank=True)
    title = models.CharField(max_length=1024,null=True,blank=True)
    short_text = models.CharField(max_length=1024,null=True,blank=True)
    image = models.ImageField(upload_to='static/image/project/%Y/%m/',null=True,blank=True,verbose_name=u"Изображение")
    role  = models.CharField(max_length=255,null=True,blank=True)
    dt    = models.DateTimeField(auto_now_add=True)
    description = models.TextField(null=True,blank=True,verbose_name=u"Описание")
    tehs    = models.TextField(null=True,blank=True,verbose_name=u"Используемые технологии")
    is_open = models.BooleanField(default=False,verbose_name=u"Открытый проект")
    on_main = models.BooleanField(default=True,verbose_name=u"Показывать на странице профиля")
    def __unicode__(self):
        return self.title
    
    class Meta:
        ordering=("-id",)
        
    def mans(self):
        return ManProject.objects.filter(project=self,is_active=True)
        
    def candidates(self):
        return ManProject.objects.filter(project=self,is_active=False)
        
    def get_image_url(self):
        if self.man==None: return 
        if self.image==None or self.image=="": return self.man.get_image_url()
        return "/"+self.image.url
        
    def get_absolute_url(self):
        return reverse('project',args=[self.id])
    
    def comments(self):
        return ProjectComment.objects.filter(project=self).order_by("id")
    
    def getLink(self):
        if self.link[:4].lower()=="http": return self.link
        return u"http://"+self.link
    
class ManProject(models.Model):
    man       = models.ForeignKey(Man)
    project   = models.ForeignKey(Project)
    dt        = models.DateTimeField(auto_now_add=True)
    role      = models.CharField(max_length=255,null=True,blank=True)
    is_active = models.BooleanField(default=True,verbose_name=u"В проекте")


class ProjectComment(models.Model):
    user    = models.ForeignKey(User)
    project = models.ForeignKey(Project)
    dt      = models.DateTimeField(auto_now_add=True)
    text    = models.TextField()


class Navik(models.Model):
    name = models.CharField(max_length=255)
    def __unicode__(self):
        return self.name

class ManNavik(models.Model):
    man   = models.ForeignKey(Man)
    navik = models.ForeignKey(Navik)
    srok  = models.IntegerField()
    def __unicode__(self):
        return self.man.name+" - "+self.navik.name
    def badge_class(self):
        y = datetime.datetime.now().year
        srok = self.srok
        if y-srok<1: return 'badge-default'
        elif y-srok>=1 and y-srok<5: return 'badge-info'
        return 'badge-success'
    def badge_srok(self):
        y = datetime.datetime.now().year
        srok = self.srok

        if y-srok<1: return 'менее года'
        return "%s %s"%(y - srok,yearName(y-srok))
        
class Interes(models.Model):
    name = models.CharField(max_length=255)
    
class ManInteres(models.Model):
    man     = models.ForeignKey(Man)
    interes = models.ForeignKey(Interes)

class GroupActiveManager(models.Manager):
    def get_queryset(self):
        return super(GroupActiveManager, self).get_queryset().filter(is_removed=False,is_active=True)

class Group(models.Model):
    name   = models.CharField(max_length=255)
    uid    = models.CharField(max_length=255,null=True,blank=True)
    dt     = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(Man)
    image  = models.ImageField(upload_to='static/image/group/%Y/%m/',null=True,blank=True,verbose_name=u"Изображение")
    description = models.TextField(null=True,blank=True,verbose_name=u"Описание")
    src_link    = models.CharField(max_length=1024,null=True,blank=True)
    is_removed  = models.BooleanField(default=False)
    is_active   = models.BooleanField(default=False)
    
    def __unicode__(self):
        return self.name


    objects = models.Manager()
    active = GroupActiveManager()


    def topics(self):
        return GroupTopic.objects.filter(group=self).order_by("-id")[:30]
    
    def last_topic(self):
        if self.topics().count()==0: return None
        return self.topics()[0]
        
    def get_image_url(self):
        if self.image==None or self.image=="": return "/static/images/rss.png"
        return "/"+self.image.url
    
    def get_absolute_url(self):
        return reverse('group',args=[self.pk])
        
        
class ManGroup(models.Model):
    ROLE_CHOICES=(
     (0,u"Подписчик")
    ,(1,u"Администратор")
    )
    man   = models.ForeignKey(Man)
    group = models.ForeignKey(Group)
    role  = models.IntegerField(default=0,choices=ROLE_CHOICES,verbose_name=u"Роль пользователя в группе")
    dt    = models.DateTimeField(auto_now_add=True)

class GroupTopic(models.Model):
    group     = models.ForeignKey(Group,null=True,blank=True)
    author    = models.ForeignKey(Man)
    dt        = models.DateTimeField(auto_now_add=True)    
    title     = models.CharField(max_length=1024)
    text      = models.TextField()
    full_text = models.TextField(null=True,blank=True,verbose_name=u"Полный текст")
    link      = models.CharField(max_length=255,null=True,blank=True)
    xlink     = models.CharField(max_length=255,null=True,blank=True,verbose_name=u"Дополнительная ссылка")
    extra     = models.TextField(null=True,blank=True,verbose_name=u"Дополнительная техническая информация")
    tags      = models.CharField(max_length=255,null=True,blank=True,verbose_name=u"Теги")
    is_active = models.BooleanField(default=True)
    is_processed = models.BooleanField(default=False)
    def __unicode__(self):
        return "%s - %s"%(self.group.name,self.title)
    
    def get_absolute_url(self):
        return reverse('article',args=[self.pk])
    
    def makeAction(self):
        return
        if self.group==None: return
        Action.objects.filter(group=self.group).update(hide=True)
        for d in ManGroup.objects.filter(group=self.group):
            ac = Action(man=d.man)
            ac.group = self.group
            ac.image_link = self.group.get_image_url()
            ac.title = self.group.name
            ac.text  = self.title
            ac.link  = self.get_absolute_url()
            ac.save()
    
    def comments(self):
        return TopicComment.objects.filter(topic=self,hide=False).order_by("-id")

class TopicComment(models.Model):
    topic = models.ForeignKey(GroupTopic)
    man   = models.ForeignKey(Man,null=True,blank=True)
    text  = models.TextField()
    dt    = models.DateTimeField(auto_now_add=True)
    hide  = models.BooleanField(default=False)
    def makeAction(self):
        image_link = None
        uname = u"Аноним"
        
        ac = Action(man=self.topic.author)        
        if self.man!=None:
            uname = self.man.name
            image_link = self.man.get_image_url()
        
        ac.image_link = image_link        
        ac.title = u"Комментарий от %s"%uname
        ac.text = self.topic.title
        ac.link = self.topic.get_absolute_url()
        ac.save()
        
class Action(models.Model):
    man     = models.ForeignKey(Man)
    group   = models.ForeignKey(Group,null=True,blank=True)
    topic   = models.ForeignKey(GroupTopic,null=True,blank=True)
    image_link = models.CharField(max_length=1024,null=True,blank=True)
    title   = models.CharField(max_length=1024)
    text    = models.TextField(null=True,blank=True)
    link    = models.CharField(max_length=1024,null=True,blank=True)
    hide    = models.BooleanField(default=False)
    dt      = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)
    
    class Meta:
        ordering=("-id",)

class ManTopicBookmark(models.Model):
    man = models.ForeignKey(Man)
    topic = models.ForeignKey(GroupTopic)

class File(models.Model):
    man   = models.ForeignKey(Man)
    name  = models.CharField(max_length=255,null=True,blank=True,verbose_name=u"Наименование файла")
    file  = models.FileField(upload_to='static/files/man/%Y/%m/',verbose_name=u"Файл")
    title = models.CharField(max_length=255,null=True,blank=True,verbose_name=u"Описание")
    size  = models.IntegerField(default=0,verbose_name=u"Размер")
    dt    = models.DateTimeField(auto_now_add=True,verbose_name=u"Дата добавления")
    
    def is_image(self):
        return self.name[-4:].lower() in [".jpg","jpeg",".png",".gif","tiff"]
    
    def source_language(self):
        if self.name[-3:].lower()==".py": return "python"
        
    def content(self):
        return file(os.path.join(settings.BASE_DIR,self.file.name),"r").read()
        
        
class Dialog(models.Model):
    user1  = models.ForeignKey(User,related_name=u"user1",null=True,blank=True,verbose_name=u"От кого")
    user2  = models.ForeignKey(User,related_name=u"user2",verbose_name=u"Кому")
    dt     = models.DateTimeField(null=True,blank=True,verbose_name=u"Дата последнего сообщения")
    
    class Meta:
        ordering = ("-dt",)
    
    def last_message(self):
        ms = Message.objects.filter(dialog=self).order_by("-id")
        if ms.count()==0: return Message()
        return ms[0]
    
    def messages(self):
        return Message.objects.filter(dialog=self).order_by("id")

class Message(models.Model):
    dialog    = models.ForeignKey(Dialog)
    to_user   = models.ForeignKey(User,related_name='to_user',null=True,blank=True,verbose_name=u"Кому")
    from_user = models.ForeignKey(User,related_name='from_user',null=True,blank=True,verbose_name=u"От кого")
    text      = models.TextField(verbose_name=u"Сообщение")    
    dt        = models.DateTimeField(auto_now_add=True,verbose_name=u"Дата отправки")
    is_viewed = models.BooleanField(default=False,verbose_name=u"Просмотрено")
    class Meta:
        ordering = ("id",)
        
class ManPodpis(models.Model):
    man1 = models.ForeignKey(Man,related_name="man1")
    man2 = models.ForeignKey(Man,related_name="man2")
    dt = models.DateTimeField(auto_now_add=True)
    class Meta:
        ordering = ("-id",)
        
admin.site.register(Prof)
admin.site.register(Man)
admin.site.register(Project)
admin.site.register(ManProject)
admin.site.register(Navik)
admin.site.register(ManNavik)
admin.site.register(Group)
admin.site.register(ManGroup)
admin.site.register(GroupTopic)
admin.site.register(File)
