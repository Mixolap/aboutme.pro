# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0021_manproject_role'),
    ]

    operations = [
        migrations.AddField(
            model_name='man',
            name='city',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0413\u043e\u0440\u043e\u0434 \u043f\u0440\u043e\u0436\u0438\u0432\u0430\u043d\u0438\u044f', blank=True),
        ),
    ]
