# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0035_auto_20160617_1252'),
    ]

    operations = [
        migrations.CreateModel(
            name='Action',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image_link', models.CharField(max_length=1024, null=True, blank=True)),
                ('title', models.CharField(max_length=1024)),
                ('text', models.TextField(null=True, blank=True)),
                ('link', models.CharField(max_length=1024, null=True, blank=True)),
                ('hide', models.BooleanField(default=False)),
                ('dt', models.DateTimeField(auto_now_add=True)),
                ('group', models.ForeignKey(blank=True, to='psotial.Group', null=True)),
                ('man', models.ForeignKey(to='psotial.Man')),
            ],
        ),
        migrations.CreateModel(
            name='TopicComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField()),
                ('dt', models.DateTimeField(auto_now_add=True)),
                ('man', models.ForeignKey(blank=True, to='psotial.Man', null=True)),
                ('topic', models.ForeignKey(to='psotial.GroupTopic')),
            ],
        ),
    ]
