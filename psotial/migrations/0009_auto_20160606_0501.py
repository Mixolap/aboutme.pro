# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0008_auto_20160603_1237'),
    ]

    operations = [
        migrations.CreateModel(
            name='ManGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dt', models.DateTimeField(auto_now_add=True)),
                ('group', models.ForeignKey(to='psotial.Group')),
                ('man', models.ForeignKey(to='psotial.Man')),
            ],
        ),
        migrations.AlterField(
            model_name='project',
            name='dt',
            field=models.DateTimeField(default=datetime.datetime(2016, 6, 6, 5, 1, 34, 955604, tzinfo=utc)),
        ),
    ]
