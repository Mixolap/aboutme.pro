# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0038_man_is_active'),
    ]

    operations = [
        migrations.CreateModel(
            name='ManPodpis',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dt', models.DateTimeField(auto_now_add=True)),
                ('man1', models.ForeignKey(related_name='man1', to='psotial.Man')),
                ('man2', models.ForeignKey(related_name='man2', to='psotial.Man')),
            ],
        ),
    ]
