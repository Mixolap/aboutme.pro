# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0005_auto_20160531_1119'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='dt',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 31, 13, 29, 30, 378127, tzinfo=utc)),
        ),
    ]
