# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0044_auto_20160704_1628'),
    ]

    operations = [
        migrations.AddField(
            model_name='action',
            name='topic',
            field=models.ForeignKey(blank=True, to='psotial.GroupTopic', null=True),
        ),
    ]
