# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('psotial', '0051_auto_20160908_1448'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='projectcomment',
            name='man',
        ),
        migrations.AddField(
            model_name='projectcomment',
            name='user',
            field=models.ForeignKey(default=None, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
