# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('psotial', '0034_action_hide'),
    ]

    operations = [
        migrations.CreateModel(
            name='Dialog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dt', models.DateTimeField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u043f\u043e\u0441\u043b\u0435\u0434\u043d\u0435\u0433\u043e \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f', blank=True)),
                ('user1', models.ForeignKey(related_name='user1', verbose_name='\u041e\u0442 \u043a\u043e\u0433\u043e', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('user2', models.ForeignKey(related_name='user2', verbose_name='\u041a\u043e\u043c\u0443', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-dt',),
            },
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField(verbose_name='\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435')),
                ('dt', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u043e\u0442\u043f\u0440\u0430\u0432\u043a\u0438')),
                ('is_viewed', models.BooleanField(default=False, verbose_name='\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440\u0435\u043d\u043e')),
                ('dialog', models.ForeignKey(to='psotial.Dialog')),
                ('from_user', models.ForeignKey(related_name='from_user', verbose_name='\u041e\u0442 \u043a\u043e\u0433\u043e', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('to_user', models.ForeignKey(related_name='to_user', verbose_name='\u041a\u043e\u043c\u0443', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='action',
            name='group',
        ),
        migrations.RemoveField(
            model_name='action',
            name='man',
        ),
        migrations.RemoveField(
            model_name='topiccomment',
            name='man',
        ),
        migrations.RemoveField(
            model_name='topiccomment',
            name='topic',
        ),
        migrations.DeleteModel(
            name='Action',
        ),
        migrations.DeleteModel(
            name='TopicComment',
        ),
    ]
