# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0055_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='man',
            name='aboutme',
            field=models.TextField(null=True, verbose_name='\u041e\u0431\u043e \u043c\u043d\u0435', blank=True),
        ),
    ]
