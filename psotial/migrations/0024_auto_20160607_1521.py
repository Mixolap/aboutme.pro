# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0023_grouptopic_full_text'),
    ]

    operations = [
        migrations.AlterField(
            model_name='grouptopic',
            name='group',
            field=models.ForeignKey(blank=True, to='psotial.Group', null=True),
        ),
    ]
