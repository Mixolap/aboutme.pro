# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0050_auto_20160908_1447'),
    ]

    operations = [
        migrations.AlterField(
            model_name='grouptopic',
            name='title',
            field=models.CharField(max_length=1024),
        ),
    ]
