# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0027_auto_20160608_1741'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='file',
            field=models.FileField(upload_to=b'static/files/man/%Y/%m/', verbose_name='\u0424\u0430\u0439\u043b'),
        ),
    ]
