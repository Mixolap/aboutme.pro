# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0024_auto_20160607_1521'),
    ]

    operations = [
        migrations.AddField(
            model_name='grouptopic',
            name='tags',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0422\u0435\u0433\u0438', blank=True),
        ),
    ]
