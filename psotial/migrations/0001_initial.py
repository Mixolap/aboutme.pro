# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Interes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Man',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('phone', models.CharField(max_length=255, null=True, blank=True)),
                ('skype', models.CharField(max_length=255, null=True, blank=True)),
                ('icq', models.CharField(max_length=255, null=True, blank=True)),
                ('image', models.ImageField(upload_to=b'static/image/man/%Y/%m/', null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ManInteres',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('interes', models.ForeignKey(to='psotial.Interes')),
                ('man', models.ForeignKey(to='psotial.Man')),
            ],
        ),
        migrations.CreateModel(
            name='ManNavik',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('srok', models.IntegerField()),
                ('man', models.ForeignKey(to='psotial.Man')),
            ],
        ),
        migrations.CreateModel(
            name='ManProject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('role', models.IntegerField(default=1, choices=[(1, '\u043f\u0440\u043e\u0433\u0440\u0430\u043c\u043c\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u0435'), (2, '\u0434\u0438\u0437\u0430\u0439\u043d'), (3, '\u0430\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u0435'), (4, '\u0440\u0430\u0441\u043a\u0440\u0443\u0442\u043a\u0430'), (5, '\u0442\u0435\u0441\u0442\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u0435')])),
                ('man', models.ForeignKey(to='psotial.Man')),
            ],
        ),
        migrations.CreateModel(
            name='Navik',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('link', models.CharField(max_length=255, null=True, blank=True)),
                ('title', models.CharField(max_length=1024)),
                ('image', models.ImageField(upload_to=b'static/image/project/%Y/%m/', null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='manproject',
            name='project',
            field=models.ForeignKey(to='psotial.Project'),
        ),
        migrations.AddField(
            model_name='mannavik',
            name='navik',
            field=models.ForeignKey(to='psotial.Navik'),
        ),
    ]
