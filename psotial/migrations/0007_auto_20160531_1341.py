# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0006_project_dt'),
    ]

    operations = [
        migrations.CreateModel(
            name='Prof',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.AlterField(
            model_name='project',
            name='dt',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 31, 13, 41, 49, 258378, tzinfo=utc)),
        ),
        migrations.AddField(
            model_name='man',
            name='prof',
            field=models.ForeignKey(blank=True, to='psotial.Prof', null=True),
        ),
    ]
