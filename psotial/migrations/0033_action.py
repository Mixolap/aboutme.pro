# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0032_topiccomment'),
    ]

    operations = [
        migrations.CreateModel(
            name='Action',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image_link', models.CharField(max_length=1024, null=True, blank=True)),
                ('title', models.CharField(max_length=1024, null=True, blank=True)),
                ('text', models.TextField(null=True, blank=True)),
                ('link', models.CharField(max_length=1024, null=True, blank=True)),
                ('dt', models.DateTimeField(auto_now_add=True)),
                ('group', models.ForeignKey(blank=True, to='psotial.Group', null=True)),
                ('man', models.ForeignKey(to='psotial.Man')),
            ],
            options={
                'ordering': ('-id',),
            },
        ),
    ]
