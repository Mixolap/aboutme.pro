# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0011_auto_20160606_0530'),
    ]

    operations = [
        migrations.AddField(
            model_name='grouptopic',
            name='link',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='dt',
            field=models.DateTimeField(default=datetime.datetime(2016, 6, 6, 5, 45, 44, 819446, tzinfo=utc)),
        ),
    ]
