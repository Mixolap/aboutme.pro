# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0042_group_is_removed'),
    ]

    operations = [
        migrations.AddField(
            model_name='action',
            name='is_read',
            field=models.BooleanField(default=False),
        ),
    ]
