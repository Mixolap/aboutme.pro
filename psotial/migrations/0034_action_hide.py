# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0033_action'),
    ]

    operations = [
        migrations.AddField(
            model_name='action',
            name='hide',
            field=models.BooleanField(default=False),
        ),
    ]
