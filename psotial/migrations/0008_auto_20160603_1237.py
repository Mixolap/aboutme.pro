# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0007_auto_20160531_1341'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('uid', models.CharField(max_length=255, null=True, blank=True)),
                ('dt', models.DateTimeField(auto_now_add=True)),
                ('author', models.ForeignKey(to='psotial.Man')),
            ],
        ),
        migrations.CreateModel(
            name='GroupTopic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dt', models.DateTimeField(auto_now_add=True)),
                ('title', models.CharField(max_length=255)),
                ('text', models.TextField()),
                ('author', models.ForeignKey(to='psotial.Man')),
                ('group', models.ForeignKey(to='psotial.Group')),
            ],
        ),
        migrations.AlterField(
            model_name='project',
            name='dt',
            field=models.DateTimeField(default=datetime.datetime(2016, 6, 3, 12, 37, 42, 442231, tzinfo=utc)),
        ),
    ]
