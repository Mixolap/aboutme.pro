# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0041_group_src_link'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='is_removed',
            field=models.BooleanField(default=False),
        ),
    ]
