# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0012_auto_20160606_0545'),
    ]

    operations = [
        migrations.AddField(
            model_name='grouptopic',
            name='xlink',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0414\u043e\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u044c\u043d\u0430\u044f \u0441\u0441\u044b\u043b\u043a\u0430', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='dt',
            field=models.DateTimeField(default=datetime.datetime(2016, 6, 6, 6, 15, 8, 220951, tzinfo=utc)),
        ),
    ]
