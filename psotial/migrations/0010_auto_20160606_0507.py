# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0009_auto_20160606_0501'),
    ]

    operations = [
        migrations.AddField(
            model_name='man',
            name='interes',
            field=models.TextField(null=True, verbose_name='\u0427\u0435\u043c \u0438\u043d\u0442\u0435\u0440\u0435\u0441\u0443\u0435\u0442\u0441\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='dt',
            field=models.DateTimeField(default=datetime.datetime(2016, 6, 6, 5, 7, 7, 848547, tzinfo=utc)),
        ),
    ]
