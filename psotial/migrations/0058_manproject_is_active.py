# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0057_project_is_open'),
    ]

    operations = [
        migrations.AddField(
            model_name='manproject',
            name='is_active',
            field=models.BooleanField(default=True, verbose_name='\u0412 \u043f\u0440\u043e\u0435\u043a\u0442\u0435'),
        ),
    ]
