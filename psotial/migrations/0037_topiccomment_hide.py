# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0036_action_topiccomment'),
    ]

    operations = [
        migrations.AddField(
            model_name='topiccomment',
            name='hide',
            field=models.BooleanField(default=False),
        ),
    ]
