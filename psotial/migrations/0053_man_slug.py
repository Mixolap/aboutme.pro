# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0052_auto_20160908_1454'),
    ]

    operations = [
        migrations.AddField(
            model_name='man',
            name='slug',
            field=models.SlugField(null=True, blank=True),
        ),
    ]
