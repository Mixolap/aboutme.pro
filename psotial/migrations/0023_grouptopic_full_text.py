# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0022_man_city'),
    ]

    operations = [
        migrations.AddField(
            model_name='grouptopic',
            name='full_text',
            field=models.TextField(null=True, verbose_name='\u041f\u043e\u043b\u043d\u044b\u0439 \u0442\u0435\u043a\u0441\u0442', blank=True),
        ),
    ]
