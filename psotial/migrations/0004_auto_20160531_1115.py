# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0003_project_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='manproject',
            name='role',
            field=models.CharField(max_length=255),
        ),
    ]
