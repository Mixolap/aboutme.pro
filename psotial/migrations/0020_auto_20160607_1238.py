# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0019_auto_20160607_1237'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='dt',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
