# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0045_action_topic'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='message',
            options={'ordering': ('id',)},
        ),
    ]
