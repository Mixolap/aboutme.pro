# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0020_auto_20160607_1238'),
    ]

    operations = [
        migrations.AddField(
            model_name='manproject',
            name='role',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
