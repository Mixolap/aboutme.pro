# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0004_auto_20160531_1115'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='manproject',
            name='man',
        ),
        migrations.RemoveField(
            model_name='manproject',
            name='project',
        ),
        migrations.AddField(
            model_name='project',
            name='man',
            field=models.ForeignKey(blank=True, to='psotial.Man', null=True),
        ),
        migrations.AddField(
            model_name='project',
            name='role',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='title',
            field=models.CharField(max_length=1024, null=True, blank=True),
        ),
        migrations.DeleteModel(
            name='ManProject',
        ),
    ]
