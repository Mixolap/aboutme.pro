# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0040_auto_20160621_1657'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='src_link',
            field=models.CharField(max_length=1024, null=True, blank=True),
        ),
    ]
