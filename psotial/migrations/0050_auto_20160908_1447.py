# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0049_auto_20160831_0931'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dt', models.DateTimeField(auto_now_add=True)),
                ('text', models.TextField()),
                ('man', models.ForeignKey(to='psotial.Man')),
                ('project', models.ForeignKey(to='psotial.Project')),
            ],
        ),
        migrations.AlterField(
            model_name='grouptopic',
            name='title',
            field=models.CharField(max_length=1024),
        ),
    ]
