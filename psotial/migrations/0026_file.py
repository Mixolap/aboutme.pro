# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0025_grouptopic_tags'),
    ]

    operations = [
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FileField(upload_to=b'static/files/man/%Y/%m/', verbose_name='\u0424\u0430\u0439\u043b')),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('size', models.IntegerField(default=0, verbose_name='\u0420\u0430\u0437\u043c\u0435\u0440')),
                ('dt', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u044f')),
                ('man', models.ForeignKey(to='psotial.Man')),
            ],
        ),
    ]
