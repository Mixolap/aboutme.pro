# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0046_auto_20160705_2238'),
    ]

    operations = [
        migrations.AddField(
            model_name='man',
            name='activation_uid',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041a\u043b\u044e\u0447 \u0430\u043a\u0442\u0438\u0432\u0430\u0446\u0438\u0438', blank=True),
        ),
    ]
