# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0039_manpodpis'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='manpodpis',
            options={'ordering': ('-id',)},
        ),
        migrations.AddField(
            model_name='project',
            name='short_text',
            field=models.CharField(max_length=1024, null=True, blank=True),
        ),
    ]
