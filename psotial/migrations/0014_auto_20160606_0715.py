# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0013_auto_20160606_0615'),
    ]

    operations = [
        migrations.CreateModel(
            name='ManTopicBookmark',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('man', models.ForeignKey(to='psotial.Man')),
                ('topic', models.ForeignKey(to='psotial.GroupTopic')),
            ],
        ),
        migrations.AlterField(
            model_name='project',
            name='dt',
            field=models.DateTimeField(default=datetime.datetime(2016, 6, 6, 7, 15, 10, 343913, tzinfo=utc)),
        ),
    ]
