# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0031_grouptopic_is_active'),
    ]

    operations = [
        migrations.CreateModel(
            name='TopicComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_removed', models.BooleanField(default=False)),
                ('text', models.TextField()),
                ('dt', models.DateTimeField(auto_now_add=True)),
                ('man', models.ForeignKey(blank=True, to='psotial.Man', null=True)),
                ('topic', models.ForeignKey(to='psotial.GroupTopic')),
            ],
        ),
    ]
