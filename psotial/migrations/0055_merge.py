# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0049_auto_20160912_2107'),
        ('psotial', '0054_group_is_active'),
    ]

    operations = [
    ]
