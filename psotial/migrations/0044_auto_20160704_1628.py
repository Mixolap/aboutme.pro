# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0043_action_is_read'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='action',
            options={'ordering': ('-id',)},
        ),
        migrations.AddField(
            model_name='grouptopic',
            name='is_processed',
            field=models.BooleanField(default=False),
        ),
    ]
