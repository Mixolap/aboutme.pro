# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0030_file_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='grouptopic',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
