# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0037_topiccomment_hide'),
    ]

    operations = [
        migrations.AddField(
            model_name='man',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
    ]
