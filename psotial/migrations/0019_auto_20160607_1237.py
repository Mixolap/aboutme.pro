# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0018_auto_20160607_1230'),
    ]

    operations = [
        migrations.CreateModel(
            name='ManProject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dt', models.DateTimeField(auto_now_add=True)),
                ('man', models.ForeignKey(to='psotial.Man')),
            ],
        ),
        migrations.AlterField(
            model_name='project',
            name='dt',
            field=models.DateTimeField(default=datetime.datetime(2016, 6, 7, 8, 37, 47, 95442, tzinfo=utc)),
        ),
        migrations.AddField(
            model_name='manproject',
            name='project',
            field=models.ForeignKey(to='psotial.Project'),
        ),
    ]
