# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('psotial', '0056_man_aboutme'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='is_open',
            field=models.BooleanField(default=False, verbose_name='\u041e\u0442\u043a\u0440\u044b\u0442\u044b\u0439 \u043f\u0440\u043e\u0435\u043a\u0442'),
        ),
    ]
