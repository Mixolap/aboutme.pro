"""psotial URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required
import views 
import groups
import api 
import files
import message
from views import *
from groups import *
from files import *


urlpatterns = [
    url(r'^$', LoginView.as_view(), name='login'),
    url(r'^logout/$', views.logoutUser, name='logout'),
    url(r'^activate/(?P<code>\d+)/$', views.activateMan, name='activate_man'),
    url(r'^reset/password/$', views.resetPassword, name='reset_password'),
    url(r'^reset/password/(?P<acode>\d+)/$', views.resetPassword, name='reset_password'),
    
    url(r'^ip/$', api.GetIPView.as_view(), name='logout'),
    
    url(r'^p(?P<pk>\d+)$', ManView.as_view(), name='man'),    
    url(r'^g(?P<pk>\d+)$', GroupView.as_view(), name='group'),
    url(r'^a(?P<pk>\d+)$', ArticleView.as_view(), name='article'),
    url(r'^a/new/$', AddArticleView.as_view(), name='add_article'), # groups
    url(r'^news/$', NewsView.as_view(), name='news'),
    
    
    url(r'^pr(?P<pk>\d+)$', ProjectView.as_view(), name='project'),
    url(r'^pr/add/$', login_required(AddProjectView.as_view()), name='add_project'),
    url(r'^pr(?P<pk>\d+)/edit/$', login_required(AddProjectView.as_view()), name='edit_project'),
    url(r'^pr(?P<pk>\d+)/want/$', WantToProjectView.as_view(), name='want_to_project'),
    url(r'^projects/$', ProjectsView.as_view(), name='projects'),
    url(r'^projects/(?P<slug>\w+)/$', ProjectsView.as_view(), name='projects'),
    
    url(r'^podpis/man/(?P<pk>\d+)/$', PodpisMan.as_view(), name='podpis_man'),
    
    url(r'^action/(?P<pk>\d+)/hide/$', ActionView.as_view(), name='hide_action'),
    url(r'^comment/(?P<pk>\d+)/hide/$', CommentView.as_view(), name='hide_comment'),
    
    url(r'^groups/$', AllGroupsView.as_view(), name='groups'),
    url(r'^p(?P<pk>\d+)/edit/$', EditProfileView.as_view(), name='edit_profile'),
    url(r'^p(?P<pk>\d+)/naviks/edit/$', NavikListView.as_view(), name='edit_naviks'),
    
    
    url(r'^files/$', FilesView.as_view(), name='files'),
    url(r'^file(?P<pk>\d+)$', FileView.as_view(), name='file'),
    url(r'^file/delete/(?P<pk>\d+)$', files.deleteFile , name='delete_file'),
    
    url(r'^group/mark/(?P<pk>\d+)$', groups.mark_topic, name='mark_topic'),
    url(r'^topic/add/$', groups.add_topic, name='add_topic'),
    url(r'^topic/edit/(?P<pk>\d+)/$', groups.add_topic, name='edit_topic'),
    url(r'^topic/remove/(?P<pk>\d+)/$', groups.remove_topic, name='remove_topic'),
    url(r'^topic/show/(?P<pk>\d+)/$', groups.show_topics, name='show_topics'),
    
    #url(r'^search/$', views.search, name='search'),
    url(r'^search/$', SearchView.as_view(), name='search'),
    
    url(r'^api/group/topic/import/$', api.importGroupTopics, name='import_group_topics'),
    url(r'^api/actions/new/$', api.ActionsViews.as_view(), name='api_actions'),
    url(r'^api/messages/new/$', api.MessagesViews.as_view(), name='api_new_messages'),
    
    #url(r'^message/advert/(?P<id>\d+)/$', message.messageAdvert, name='message_advert'),    
    url(r'^messages/$', message.DialogView.as_view(), name='messages'),
    url(r'^send/message/(?P<id>\d+)/$', message.sendMessage, name='send_message'),
    url(r'^dialog/(?P<id>\d+)/$', message.dialog, name='dialog'),
    url(r'^reply/(?P<id>\d+)/$', message.reply, name='reply'),
    
    url(r'^admin/', include(admin.site.urls)),
    url(r'^(?P<slug>\w+)/projects/$', ManProjectView.as_view(), name='man_projects'),
    url(r'^(?P<slug>\w+)/$', ManView.as_view(), name='man'),
]
