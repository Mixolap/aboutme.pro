#-*- encoding: utf-8 -*-
import datetime

from django.shortcuts import render
from django.http import HttpResponseRedirect,HttpResponse,HttpResponseForbidden
from django.core.urlresolvers import reverse,reverse_lazy

from django.views.generic import DetailView
from django.views.generic import View

from django.contrib.auth.models import User
from django import forms

from models import *

from django.views.generic import DetailView,ListView

class UploadFileForm(forms.ModelForm):
    class Meta:
        model = File
        exclude = ['dt','size']

class FileView(DetailView):
    model = File
    template_name = "files/file.html"
    

    
class FilesView(ListView):
    template_name = "files/files.html"
    
    def get_context_data(self,**kwargs):
        context = super(FilesView,self).get_context_data(**kwargs)
        context['form'] = UploadFileForm()
        return context
    
    def get_queryset(self):
        return File.objects.filter(man=self.request.user.man)
    
    def post(self,request,*args,**kwargs):
        if not request.user.is_authenticated(): return HttpResponseForbidden()
        form = UploadFileForm(request.POST,request.FILES)
        if form.is_valid():
            instance = File(file=request.FILES['file'])
            instance.man   = request.user.man
            instance.title = request.POST.get("title")
            instance.name  = request.FILES['file'].name
            instance.size  = request.FILES['file'].size
            instance.save()
        return HttpResponseRedirect(reverse('files'))

def deleteFile(request,pk):
    File.objects.filter(pk=pk,man=request.user.man).delete()
    return HttpResponseRedirect(reverse('files'))
        
        
        
    
