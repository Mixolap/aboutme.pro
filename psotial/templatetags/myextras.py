#-*- encoding: utf-8 -*-
from django import template
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
from django.template.defaultfilters import urlencode

register = template.Library()

@register.filter
def split_naviks(value):
    if value==None: return ""
    vals = value.split(",")
    data = []
    for d in vals:
        data.append("<a href='/search/?interes=%s'>%s</a>"%(conditional_escape(urlencode(d.strip())),conditional_escape(d.strip())))
    return mark_safe(", ".join(data))
    
@register.filter
def split_tags(value):
    if value==None: return ""
    vals = value.split(",")
    data = []
    for d in vals:
        data.append("<a href='/search/?tag=%s'>%s</a>"%(conditional_escape(urlencode(d.strip())),conditional_escape(d.strip())))
    return mark_safe(", ".join(data))

@register.filter
def split_tehs(value):
    if value==None: return ""
    vals = value.split(",")
    data = []
    for d in vals:
        data.append("<a href='/search/?teh=%s'>%s</a>"%(conditional_escape(urlencode(d.strip())),conditional_escape(d.strip())))
    return mark_safe(", ".join(data))


mailservers = {

}    

@register.filter
def mailserver(value):
    if value==None: return ""
    vals = value.split("@")
    if len(vals)!=2: return ""
    
    if vals[1] in mailservers.keys(): return mailservers[vals[1]]
    return "https://mail."+vals[1]
    
    
    