#-*- encoding: utf-8 -*-
import datetime
import random 

from django.shortcuts import render
from django.http import HttpResponseRedirect,HttpResponse,HttpResponseForbidden

from django.core.urlresolvers import reverse

from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import TemplateView
from django.views.generic.base import ContextMixin
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.decorators import login_required
from django import forms

from send_signal import send_signal
from django.core.mail import send_mail

from django.contrib.auth.models import User
from models import *
from funcs import *

def getProf(name):
    if name==None: return
    name = name.strip()
    pf = Prof.objects.filter(name__iexact=name)
    if pf.count()>0: return pf[0]
    return Prof.objects.create(name=name)
    
def getNavik(name):
    if name==None: return
    name = name.strip()
    pf = Navik.objects.filter(name__iexact=name)
    if pf.count()>0: return pf[0]
    return Navik.objects.create(name=name)

## отправить письмо пользователю man
def sendMail(man,title,text):
    email = man.user.email
    title = unicode(title).encode('utf-8')
    text  = unicode(text).encode('utf-8')
    send_mail(title, '', 'noreply@aboutme.pro',[email], fail_silently=False,html_message=text)        


class ManView(DetailView):
    model = Man
    template_name = "man.html"
    def get_context_data(self,**kwargs):
        context = super(ManView,self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            context['have_podpis'] = ManPodpis.objects.filter(man1=self.request.user.man,man2=self.get_object()).count()>0
        context['selected_man'] = self.get_object()
        return context
    

class AuthenticationNoCheckPasswordBackend(ModelBackend):
    def authenticate(self, username=None, password=None):
        try:
            user = User.objects.get(username = username)
            return user
        except User.DoesNotExist:
            return None

def activateMan(request,code):
    all_good = False
    
    if Man.objects.filter(activation_uid=code).count()>0:
        object = Man.objects.filter(activation_uid=code)[0]
        object.activation_uid = None
        object.is_active = True
        object.save()
        
        #if request.user.is_authenticated(): logout(request)
        #if not request.user.is_authenticated():
        #    login(request,object.user)
        #login(request,object.user,backend=AuthenticationNoCheckPasswordBackend)
        text  = u'Аккаунт почтового ящика %s на сайте aboutme.pro активирован'
        email = object.user.username
        send_mail(u'Ваш аккаунт на сайте aboutme.pro активирован', text, 'noreply@aboutme.pro',[email], fail_silently=False,html_message=text)
        good_msg = u'Ваш аккаунт активирован, выполните вход на сайт'
        return HttpResponseRedirect("/")
    
    return HttpResponseRedirect('/') 

def resetPassword(request,acode='',template_name="reset_password.html"):
    ResetPassword.objects.filter(dt__lte=timezone.now()-datetime.timedelta(days=10)).delete()
    if request.method=="POST":
        all_good = True
        email = request.POST.get("email")
        acode = genPasswordActivationCode()
        ResetPassword.objects.filter(email=email).delete()
        ResetPassword(email=email,uid=acode).save()
        text = u'Для смены пароля перейдите по ссылке <a href="'+settings.SITE_URL+'/reset/password/'+acode+'/">'+settings.SITE_URL+'/reset/password/'+acode+'/</a>'
        try:
            send_mail(u'Сброс пароля на сайте aboutme.pro', text, 'noreply@aboutme.pro',[email], fail_silently=False,html_message=text)
        except: pass
        new_password = False
        return render(request,template_name,locals())
    
    if acode!='':
        rp = ResetPassword.objects.filter(uid=acode)
        if rp.count()==0:
            badacode = True
            return render(request,template_name,locals())
        else:
            rp = rp[0]
            if User.objects.filter(email=rp.email).count()==0:
                no_user = True
                email = rp.email
                return render(request,template_name,locals())
            user = User.objects.get(email=rp.email)
            newpassword = genPassword()
            user.set_password(newpassword)
            user.save()
            new_password = True
            rp.delete()
            user = authenticate(username=rp.email,password=newpassword)
            login(request,user)
            
            return render(request,template_name,locals())
            
    email = request.GET.get("email","")
    return render(request,template_name,locals())

    
class LoginView(TemplateView):
    template_name = "login.html"
    msg = ''
    username = ''
    def get_context_data(self,**kwargs):
        context = super(LoginView,self).get_context_data(**kwargs)
        context['last_users']    = Man.objects.filter(prof__isnull=False,image__isnull=False).exclude(image="").order_by("-id")[:6]
        context['last_groups']   = Group.active.all().order_by("-id")[:6]
        context['last_projects'] = Project.objects.filter(image__isnull=False).order_by("-id")[:6]
        context['msg'] = self.msg
        context['email'] = self.username
        return context
    
    def genActivationCode(self):
        code = ""
        for i in xrange(10):
            d = random.randint(0,9)
            code += str(d)
            
        while Man.objects.filter(activation_uid=code).count()>0:
            return genActivationCode()
        return code
        
    def get(self,request,*args,**kwargs):
        if request.user.is_authenticated(): return HttpResponseRedirect(request.user.man.get_absolute_url())
        if request.GET.get("msg")=="1":
            self.msg = u"Для выполнения этого действия вам необходимо авторизоваться"
        return super(LoginView,self).get(request,*args,**kwargs)
    
    def post(self,request,*args,**kwargs):
        self.username = ''
        if request.user.is_authenticated(): return HttpResponseRedirect(request.user.man.get_absolute_url())
        r_username = request.POST.get("r_username","")
        if r_username!="":
            if User.objects.filter(username=r_username).count()>0:
                self.msg=r_username+u" уже зарегистрирован"
                return super(LoginView,self).get(request,*args,**kwargs)
                
            u = User.objects.create_user(username=r_username,password="1")
            u.is_active = True
            u.save()
            m = Man(user=u,name=u"укажите своё имя",activation_uid=self.genActivationCode())
            # временно убираем активацию пользователя
            m.activation_uid = None
            m.is_active = True
            
            m.save()
            luser = authenticate(username=r_username, password="1")
            luser.email = r_username
            luser.save()
            
            login(request,luser)

            send_signal(0,u'Новая регистрация на aboutme.pro')
            
            """
            text = u'Для активации аккаунта перейдите по ссылке <a href="http://aboutme.pro/activate/'+m.activation_uid+u'/"> http://aboutme.pro/activate/'+m.activation_uid+u'/</a>'
            
            try:
                send_mail(u'Активация аккаунта на сайте aboutme.pro', text, 'noreply@aboutme.pro',[r_username], fail_silently=False,html_message=text)
            except: pass
            """
            
            return HttpResponseRedirect(reverse("edit_profile",args=[m.id]))
            
        username = request.POST.get("username")
        password = request.POST.get("password")
        luser = authenticate(username=username, password=password)
        if luser==None: 
            self.msg=u"Неверный E-Mail или пароль"
            self.username = request.POST.get("username")
            return super(LoginView,self).get(request,*args,**kwargs)
        
        login(request,luser)
        return HttpResponseRedirect(luser.man.get_absolute_url())


@login_required
def logoutUser(request):
    logout(request)
    return HttpResponseRedirect(request.GET.get("next","/"))


class EditProfileView(DetailView):
    model = Man
    template_name = "edit_profile.html"
    def post(self,request,*args,**kwargs):
        object = self.get_object()
        if not request.user.is_authenticated() or object.user != request.user: return HttpResponseForbidden()
        
        object.city    = request.POST.get("city","").strip()
        object.prof    = getProf(request.POST.get("prof"))
        
        if object.name.lower()!=request.POST.get("name","").strip().lower():
            object.name    = request.POST.get("name","").strip()
            object.slug    = object.getNewSlug()
        
        object.name    = request.POST.get("name","").strip()
        object.aboutme = request.POST.get("aboutme","").strip()
        
        if "image" in request.FILES.keys(): object.image = request.FILES["image"]
        object.save()
        
        if request.POST.get("password","")!="":
            user = request.user
            user.set_password(request.POST.get("password"))
            user.save()
            update_session_auth_hash(request, user)
        
        return HttpResponseRedirect(reverse('edit_naviks',args=[object.pk]))#object.get_absolute_url())


class NavikListView(ListView):    
    template_name = "mannavik_list.html"
    
    def get_context_data(self,**kwargs):
        context = super(NavikListView,self).get_context_data(**kwargs)
        context['object'] = self.request.user.man
        return context
        
    def get_queryset(self):
        return ManNavik.objects.filter(man_id=self.request.user.man.id)
        
    def post(self,request,*args,**kwargs):
        if not request.user.is_authenticated(): return HttpResponseForbidden()
        man = request.user.man
        man.interes = request.POST.get("interes","")
        man.save()
        
        ManNavik.objects.filter(man=man).delete()
        
        for i in xrange(5):
            nname = request.POST.get("nname%d"%i,"").strip()
            nyear = request.POST.get("nyear%d"%i,"").strip()
            if nname=="" or nyear=="": continue
            ManNavik(man=man,navik=getNavik(nname),srok=nyear).save()
                
        # взять последнюю 1000 сообщений и выбрать подходящие по интересам, если их нет в Action
        i = 0
        for gt in GroupTopic.objects.filter(is_active=True).order_by("-id")[:500]:            
            if i>=10: continue
            if Action.objects.filter(man=man,topic=gt).count()>0: continue
            if not isInterestingPost(man,gt): continue
            createAction(man,gt)
            i += 1
        return HttpResponseRedirect(man.get_absolute_url())

## просмотр проекта.    
class ProjectView(DetailView):
    model = Project
    template_name = "project.html"
    
    def get_context_data(self,**kwargs):
        context = super(ProjectView,self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            man     = self.request.user.man
            project = self.get_object()
            context['user_in_project'] = Project.objects.filter(man=man,pk=project.id).count()+ManProject.objects.filter(man=man,project=project).count()
            
        if self.request.GET.get("msg")=="1":
            context['msg'] = u"Автор проекта оповещён о вашей заявке, он скоро с вами свяжется."
            
        return context
    
    def may_edit(self):
        return self.request.user.is_authenticated() or request.user!=object.man.user
        
    def get(self,request,*args,**kwargs):
        object = self.get_object()

        if request.GET.get("rmcomment")!=None and self.may_edit():
            ProjectComment.objects.filter(project=object,pk=request.GET.get("rmcomment")).delete()
            return HttpResponseRedirect(reverse('project',args=[object.pk]))
        
        if request.GET.get("rmman")!=None:            
            if not self.may_edit(): return HttpResponseRedirect(object.get_absolute_url())            
            ManProject.objects.filter(project=object,pk=request.GET.get("rmman")).delete()
            return HttpResponseRedirect(object.get_absolute_url())

        if request.GET.get("commit")!=None:
            if not self.may_edit(): return HttpResponseRedirect(object.get_absolute_url())
            ManProject.objects.filter(project=object,pk=request.GET.get("commit")).update(is_active=True)
            return HttpResponseRedirect(object.get_absolute_url())
        return super(ProjectView,self).get(request,*args,**kwargs)
        
    def post(self,request,*args,**kwargs):
        if request.user.is_anonymous(): return HttpResponseRedirect('anonymous')
        object = self.get_object()
        if not self.may_edit(): return HttpResponseForbidden()
        
        # комментарий
        if request.POST.get("comment-text","")!="":
            ProjectComment.objects.create(user=request.user,project=object,text=request.POST.get("comment-text"))
            send_signal(0,u'Новый комментарий http://aboutme.pro'+object.get_absolute_url())
        # участники
        for d in object.man.podpis1():
            if request.POST.get("man%d"%d.man2.id)!=None:
                ManProject.objects.get_or_create(project=object,man=d.man2,role=request.POST.get("role"))
        
        for d in object.man.podpis2():
            if request.POST.get("man%d"%d.man1.id)!=None:
                mp,cr = ManProject.objects.get_or_create(project=object,man=d.man1)
                mp.role=request.POST.get("role")
                mp.save()
                
        return HttpResponseRedirect(object.get_absolute_url())
   

class ProjectForm(forms.ModelForm):
    class Meta:
        model   = Project
        exclude = ['dt']


class AddProjectView(TemplateView):
    template_name="edit_project.html"
    form_class = ProjectForm
    
    def get(self,request,*args,**kwargs):
        current_user = request.user
        if 'pk' in kwargs:
            instance = self.form_class.Meta.model.objects.get(pk=kwargs['pk'])
            if instance.man!=request.user.man: return HttpResponseForbidden()
            if request.GET.get("rm")=="1":
                instance.delete()
                return HttpResponseRedirect(request.user.man.get_absolute_url())
            form = self.form_class(instance=instance)
        else:
            form = self.form_class(initial={"man":str(request.user.man.id)})
        return render(request,self.template_name,locals())
        
    def post(self,request,*args,**kwargs):
        if not request.user.is_authenticated(): return HttpResponseForbidden()
        if 'pk' in kwargs:
            form = self.form_class(request.POST,request.FILES,instance=self.form_class.Meta.model.objects.get(pk=kwargs['pk']))
        else:
            form = self.form_class(request.POST,request.FILES)
        
        if form.is_valid():
            if form.cleaned_data['man']==request.user.man:
                form.save()
            if 'pk' in kwargs: return HttpResponseRedirect(reverse("project",args=[kwargs['pk']]))
            return HttpResponseRedirect(request.user.man.get_absolute_url())
        return render(request,self.template_name,locals())


class ActionView(DetailView):
    model = Action
    def get(self,request,*args,**kwargs):
        object = self.get_object()
        
        if object.man == request.user.man:
            object.hide=True
            object.save()
        
        return HttpResponse('200')


class SearchView(TemplateView):
    template_name="search_topic.html"
    template_name2="search_man.html"
    template_name3="search_project.html"
    def get(self,request,*args,**kwargs):
        text    = request.GET.get("search_text","").strip()
        interes = request.GET.get("interes","").strip()
        prof    = request.GET.get("prof","").strip()
        tag     = request.GET.get("tag","").strip()
        teh     = request.GET.get("teh","").strip()
        
        search_text = text
        # поиск статей по заголовку и по тексту
        if text!="":
            sr1 = GroupTopic.objects.filter(title__icontains=text,is_active=True).order_by("-id")
            sr2 = GroupTopic.objects.filter(text__icontains=text,is_active=True).exclude(pk__in=[d.pk for d in sr1]).order_by("-id")
            rcount = sr1.count()+sr2.count()
            return render(request,self.template_name,locals())
        # поиск людей и статей по интересам
        if interes!="":
            nk = Navik.objects.filter(name__iexact=interes.lower())
            sr1 = []
            if nk.count()>0:
                sr1 = [d.man for d in ManNavik.objects.filter(man__is_active=True,navik=nk[0])]
            
            sr2 = Man.objects.filter(is_active=True,interes__icontains=interes.lower())
            rcount = len(sr1)+sr2.count()
            
            tsr1 = GroupTopic.objects.filter(title__icontains=interes,is_active=True).order_by("-id")
            #tsr2 = GroupTopic.objects.filter(text__icontains=interes,is_active=True).exclude(pk__in=[d.pk for d in tsr1]).order_by("-id")
            rcount2 = tsr1.count()#+tsr2.count()
            search_text = interes
            return render(request,self.template_name2,locals())
        # поиск людей по профессии
        if prof!="":
            prof = Prof.objects.filter(name__iexact=prof.lower())
            sr1 = []
            rcount = 0
            if prof.count()>0:
                sr1 = Man.objects.filter(prof=prof[0].id)
                rcount = sr1.count()
                search_text = prof[0].name
            return render(request,self.template_name2,locals())            
        # поиск статей по тегу
        if tag!="":
            sr1 = GroupTopic.objects.filter(tags__icontains=tag,is_active=True).order_by("-id")
            rcount = sr1.count()
            search_text = tag
            return render(request,self.template_name,locals())       

        # поиск проектов по технологии
        if teh!="":
            sr1 = Project.objects.filter(tehs__icontains=teh)
            rcount = sr1.count()
            sr2 = GroupTopic.objects.filter(title__icontains=teh,is_active=True).order_by("-id")    
            rcount2 = sr2.count()
            return render(request,self.template_name3,locals())  
        return render(request,self.template_name,locals())

# подписаться на человека
class PodpisMan(DetailView):
    model = Man
    def get(self,request,*args,**kwargs):
        man1 = request.user.man
        man2 = self.get_object()
        
        if man1.id!=man2.id:
            mp,cr = ManPodpis.objects.get_or_create(man1=man1,man2=man2)
            if not cr:
                mp.delete()
            
        return HttpResponseRedirect(man2.get_absolute_url())
        
        
class WantToProjectView(DetailView):
    model = Project

    def get(self,request,*args,**kwargs):
        if request.user.is_anonymous():
            return HttpResponseRedirect("/?msg=1")
        object = self.get_object()
        man = request.user.man
        ManProject.objects.get_or_create(man=man,project=object,defaults={"is_active":False})
        title = u'К вашему проекту на aboutme.pro хочет присоединиться %s'%(man.name)
        text  = u"""К проекту <a href="http://aboutme.pro%s">%s</a> хочет присоединиться %s."""%(object.get_absolute_url(),object.title,man.name)        
        sendMail(object.man,title,text)
        
        title = u'Новый участник проекта'
        text  = u'К проекту %s хочет присоединиться %s'%(object.title,man.name)
        link  = object.get_absolute_url()
        
        createAction(object.man,None,title,text,link)
        return HttpResponseRedirect(object.get_absolute_url()+"?msg=1")
        
class ManProjectView(DetailView):
    model = Man
    template_name = "man_projects.html"

## список проектов    
class ProjectsView(ListView):
    template_name = 'projects/projects.html'
    def get_context_data(self,**kwargs):
        context = super(ProjectsView,self).get_context_data(**kwargs)
        context['slug'] = self.kwargs.get('slug')
        return context
        
    def get_queryset(self):
        if self.kwargs.get('slug')=='open': return Project.objects.filter(is_open=True)
        return Project.objects.all()
        
    

