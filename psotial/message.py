#-*- encoding: utf-8 -*-

import datetime
import random
import os
import json

from django.shortcuts import render
from django.utils import timezone
from django.contrib.auth import authenticate, login, logout
from django.contrib.humanize.templatetags.humanize import naturaltime
from django.contrib.auth.models import User
from django.http import HttpResponse,HttpResponseRedirect,Http404
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.mail import send_mail
from django.db.models import Q
from django.views.generic import ListView

from models import *

@login_required
def sendMessage(request,id):
    to_user = User.objects.get(pk=id)
    if request.method=="POST":
        dlg1,cr = Dialog.objects.get_or_create(user2=to_user,user1=request.user)
        dlg2,cr = Dialog.objects.get_or_create(user1=to_user,user2=request.user)
        
        m = Message()
        m.dialog  = dlg1
        m.text    = request.POST.get("message")
        m.to_user = to_user
        m.from_user = request.user
        m.save()
        dlg1.dt=m.dt
        dlg1.save()
        
        m = Message()
        m.dialog  = dlg2
        m.text    = request.POST.get("message")
        m.to_user = to_user
        m.from_user = request.user
        m.save()
        dlg2.dt=m.dt
        dlg2.save()
    
    return HttpResponseRedirect(to_user.man.get_absolute_url())
    #return dialog(request,id)


class DialogView(ListView):
    template_name = "messages/messages.html"
    def get_queryset(self):
        return Dialog.objects.filter(user1=self.request.user)
    
    def saveMessage(self,data,request):        
        user1 = request.user
        user2 = Man.objects.get(user_id=data['user']).user        
        self.saveForDialog(user1,user2,user1,user2,data['text'])
        return self.saveForDialog(user2,user1,user1,user2,data['text'])
    
    def saveForDialog(self,user1,user2,from_user,to_user,text):
        dlg,cr = Dialog.objects.get_or_create(user1=user1,user2=user2)   
        
        m = Message()
        m.dialog    = dlg
        m.text      = text
        m.from_user = from_user
        m.to_user   = to_user
        if from_user==user1:
            m.is_viewed = True
        m.save()
        
        dlg.dt=m.dt
        dlg.save()
        return m
    
    def post(self,request,*args,**kwargs):
        
        data = json.loads(request.read())
        print 'data=',data
        if 'text' not in data.keys() or data['text']=='': return HttpResponse('[]')
        d = self.saveMessage(data,request)

        data = {
            "id":d.id,
            "dt":naturaltime(d.dt),
            "user_url":d.from_user.man.get_absolute_url(),
            "user_name":d.from_user.man.name,
            "user_img":d.from_user.man.get_image_url(),
            "text":d.text,
        }
        
        return HttpResponse(json.dumps(data))
    
    def get(self,request,*args,**kwargs):
        #Dialog.objects.all().delete()
        
        if request.GET.get("d")=="1": return self.getDialogs(request)
        if request.GET.get("user2")!=None: return self.getMessages(request.GET.get("user2"))
        if request.GET.get("user2n")!=None: return self.getNewMessages(request.GET.get("user2n"))
            
        return render(request,self.template_name,locals())
    
    def getMessages(self,did):
        data = [{
            "id":d.id,
            "dt":naturaltime(d.dt),
            "user_url":d.from_user.man.get_absolute_url(),
            "user_name":d.from_user.man.name,
            "user_img":d.from_user.man.get_image_url(),
            "text":d.text,
        } for d in Message.objects.filter(dialog_id=did)]
        Message.objects.filter(dialog_id=did).update(is_viewed=True)
        return HttpResponse(json.dumps(data))
        
    def getNewMessages(self,did):
        data = [{
            "id":d.id,
            "dt":naturaltime(d.dt),
            "user_url":d.from_user.man.get_absolute_url(),
            "user_name":d.from_user.man.name,
            "user_img":d.from_user.man.get_image_url(),
            "text":d.text,
        } for d in Message.objects.filter(dialog_id=did,is_viewed=False)]
        Message.objects.filter(dialog_id=did).update(is_viewed=True)
        return HttpResponse(json.dumps(data))
        
    def getDialogs(self,request):
        data = [{
            "id":d.id,
            "user_id":  d.user2_id,
            "user_url": d.user2.man.get_absolute_url(),
            "user_img": d.user2.man.get_image_url(),
            "user_name": d.user2.man.name,
            "last_message": d.last_message().text,
            "dt":naturaltime(d.last_message().dt),
        } for d in self.get_queryset()]
        
        return HttpResponse(json.dumps(data))
    
    
@login_required
def dialog(request,id,template_name="messages/dialog.html"):
    dlg = Dialog.objects.get(user1=request.user,user2=User.objects.get(pk=id))
    Message.objects.filter(dialog=dlg,to_user=request.user).update(is_viewed=True)
    return render(request,template_name,locals())

@login_required
def reply(request,id):
    dlg = Dialog.objects.get(pk=id)
    
    if request.method=="POST":
        from_user = request.user
        to_user = None
        
        if dlg.user2==from_user:
            to_user = dlg.user1
        else:
            to_user = dlg.user2
        
        m = Message()
        m.dialog    = dlg
        m.text      = request.POST.get("message")
        m.from_user = from_user
        m.to_user   = to_user
        m.save()
        
        dlg.dt=m.dt
        dlg.save()
        
    
    return HttpResponseRedirect(reverse("dialog",args=[id]))

