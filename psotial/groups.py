#-*- encoding: utf-8 -*-
import datetime
import json
from django.shortcuts import render
from django.http import HttpResponseRedirect,HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.humanize.templatetags.humanize import naturaltime,naturalday
from django.views.generic import DetailView,ListView,TemplateView
from django.views.generic import View

from django.contrib.auth.models import User
from models import *


class GroupView(DetailView):
    model = Group
    template_name = "group.html"
    def get_context_data(self,**kwargs):
        context = super(GroupView,self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            context['have_podpis'] = ManGroup.objects.filter(group=self.get_object(),man__user=self.request.user).count()>0
        return context    
        
    def get(self,request,*args,**kwargs):
        
        if request.GET.get("start","")!="":
            start = int(request.GET.get("start",0))
            gt = GroupTopic.objects.filter(group=self.get_object(),is_active=True).order_by("-id")[start:start+30]
            return HttpResponse(json.dumps([{
                'link': d.link,
                'xlink': d.xlink,
                'title': d.title,
                'text': d.text,
                'dt': naturalday(d.dt),      
                "dt_str": d.dt.strftime("%d.%m.%Y %H:%I"),
                'id': d.id,  
            }for d in gt]))
        
        
        if request.GET.get("podpis","")!="":
            mg,cr = ManGroup.objects.get_or_create(group=self.get_object(),man=self.request.user.man)
            if not cr:
                mg.delete()
                
        if request.GET.get("rm","")=="1":
            group = self.get_object()
            group.is_removed = not group.is_removed
            group.save()
            return HttpResponseRedirect(group.get_absolute_url())
            
        return super(GroupView,self).get(request,*args,**kwargs)
    
class AllGroupsView(ListView):
    queryset = Group.active.all()
    template_name = "groups.html"
    def post(self,request,*args,**kwargs):
        
        gr = Group(author=request.user.man)
        gr.src_link    = request.POST.get("src_link","")
        gr.description = request.POST.get("text","")
        gr.name        = request.POST.get("title","")
        gr.save()
        
        return HttpResponseRedirect(reverse('groups'))
    
class ArticleView(DetailView):
    model = GroupTopic
    template_name = "article.html"

    def get(self,request,*args,**kwargs):
        object = self.get_object()        
        if object.xlink!=None:
            return HttpResponseRedirect(object.xlink)

        return super(ArticleView,self).get(request,*args,**kwargs)
    
    def post(self,request,*args,**kwargs):
        object = self.get_object()
        if request.POST.get("comment-text","").strip()!="":            
            tc = TopicComment(topic=object)
            tc.text = request.POST.get("comment-text","").strip()
            if request.user.is_authenticated(): tc.man = request.user.man
            tc.save()
            
            tc.makeAction()
            
        return HttpResponseRedirect(object.get_absolute_url())

class AddArticleView(TemplateView):
    template_name="groups/edit_topic.html"

    def post(self,request,*args,**kwargs):
        return add_topic(request)





class CommentView(DetailView):
    model = TopicComment
    def get(self,request,*args,**kwargs):
        object = self.get_object()
        
        if object.topic.author == request.user.man:
            object.hide=True
            object.save()
            return HttpResponse('201')
        return HttpResponse('200')

class NewsView(ListView):
    template_name = "groups/news.html"
    def get_queryset(self):
        return GroupTopic.objects.filter(is_active=True).order_by("-id")
        
    def get(self,request,*args,**kwargs):
        if request.GET.get("start","")!="":
            start    = int(request.GET.get("start",0))
            start_id = int(request.GET.get("id",0))
            gt = GroupTopic.objects.filter(is_active=True)
            if start_id!=0: gt = gt.filter(pk__lte=start_id)
            
            if request.user.is_authenticated():
                gt = gt.filter(group__in=[d.group for d in ManGroup.objects.filter(man=request.user.man)])
            
            gt = gt.order_by("-id")[start:start+30]
            return HttpResponse(json.dumps([{
                'id':d.id,
                'img_url':d.group.get_image_url(),
                'group_url': d.group.get_absolute_url(),
                'link': d.link,
                'xlink': d.xlink,
                'title': d.title,
                'text': d.text,
                'dt': naturalday(d.dt),            
            }for d in gt])) 
        
        last_users    = Man.objects.filter(prof__isnull=False,image__isnull=False).exclude(image="").order_by("-id")[:6]
        last_projects = Project.objects.filter(image__isnull=False).order_by("-id")[:6]

        return render(request,self.template_name,locals()) #super(NewsView,self).get(request,*args,**kwargs)
        
def mark_topic(request,pk):
    topic = GroupTopic.objects.get(pk=pk)
    ManTopicBookmark.objects.get_or_create(man=Man.objects.get(user=request.user),topic=topic)
    return HttpResponse('ok')
    
def add_topic(request,pk=0,template_name="groups/edit_topic.html"):
    current_user = request.user 
    pk = int(pk)
    #print "pk=",pk
    if pk!=0:
        object = GroupTopic.objects.get(pk=pk)
    else:
        object = GroupTopic()
        
    if request.method!="POST":
        return render(request,template_name,locals())
        
    man = Man.objects.get(user=request.user)
    
    group_id  = int(request.POST.get("group_id",0))
    
    if group_id==0: 
        object.group = None
    else: 
        object.group = Group.objects.get(pk=group_id)
    
    object.author    = man
    object.title     = request.POST.get("title","")
    object.text      = request.POST.get("text","")
    object.link      = request.POST.get("link","")
    object.full_text = request.POST.get("full_text","")
    object.tags      = request.POST.get("tags")
    
    if request.POST.get("is_not_active")!=None:
        object.is_active = False
        
    object.save()
    
    if pk!=0: 
        return HttpResponseRedirect(reverse('article',args=[pk]))
    
    if object.group==None: 
        ManTopicBookmark(man=man,topic=object).save()
        return HttpResponseRedirect(man.get_absolute_url())
        
    return HttpResponseRedirect(reverse('group',args=[group_id]))


    

def remove_topic(request,pk):
    man = Man.objects.get(user=request.user)
    gt = GroupTopic.objects.get(pk=pk)
    if gt.group==None and gt.author==man:
        gt.delete()
        return HttpResponseRedirect(man.get_absolute_url())
        
    if ManTopicBookmark.objects.filter(man=man,topic=gt).count()>0:
        ManTopicBookmark.objects.filter(man=man,topic=gt).delete()
    else:
        group = gt.group
        if gt.author==man:
            gt.delete()
            return HttpResponseRedirect(reverse('group',args=[group.id]))
        
    return HttpResponseRedirect(man.get_absolute_url())
    
def show_topics(request,pk,template_name="topics.htm"):
    current_user = request.user
    object = Group.objects.get(pk=pk)
    start = int(request.GET.get("start",30))
    data = GroupTopic.objects.filter(group=object).order_by("-id")[start:start+30]
    return render(request,template_name,locals())
    
    
    
    
    
    
    
    
    
    