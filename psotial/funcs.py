#-*- encoding: utf-8 -*-

from psotial.models import *


def isInterestingPost(man,topic):
    for d in man.fullIntereses().lower().split(","):
        d = d.strip()
        if len(d)==0: continue
        #if d in topic.tags.lower(): return True
        if d in topic.title.lower(): return True
        if d in topic.text.lower(): return True
    return False
    


def createAction(m,gt,title='',text='',link=''):
    ac = Action(man=m,topic=gt)
    if gt!=None:
        if gt.group: 
            ac.group = gt.group
            ac.title = gt.group.name
            ac.image_link = gt.group.get_image_url()
        else:
            ac.title = gt.author.name
            ac.image_link = gt.author.get_image_url()
            
        ac.text  = gt.title
        ac.link  = gt.get_absolute_url()
    else:
        ac.title = title
        ac.text  = text
        ac.link  = link
    ac.save()
