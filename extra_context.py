#-*- encoding: utf-8 -*-
from psotial.models import Message
from django.conf import settings

def vars(request):
    messages_count = 0
    current_man = None
    if request.user.is_authenticated():
        messages_count  = Message.objects.filter(to_user=request.user,is_viewed=False).count()
        current_man = request.user.man
        
    return {
        "current_user":request.user,
        "current_man":current_man,
        "messages_count":messages_count,
        "DEBUG":settings.DEBUG,
    }


    
    
    