jQuery(function(document){
    jQuery.Shortcuts.add({type:"down", mask:"Ctrl+Enter", enableInInput:true, handler:function () {
	      if(jQuery(".send-message").size()>0) jQuery(".send-message").click()
    }}).start();
    
    //jQuery(".scrollable").css("height",jQuery(document).height()-300);
});

app.controller('ChatController', function($scope,$interval,$http){
    $scope.dialogs = [];
    $scope.messages = [];
    
    $scope.current_message = "";
    $scope.current_dialog = 0;
    $scope.current_user = 0;
    $scope.loadDialogs = function(data){            
            $http.get("?d=1").success(function(data){ 
                for(var i=0;i<data.length;i++){
                    var found = false;
                    for(var j=0;j<$scope.dialogs.length;j++){
                        if($scope.dialogs[j]['id']==data[i]['id']){
                            found = true;
                            $scope.dialogs[j]['dt']==data[i]['dt'];
                            break;
                        }
                    }                    
                    if(!found){
                        $scope.dialogs.unshift(data[i]);
                        setTimeout("jQuery('.scrollable-content').scrollTop(1024*1024);",100);
                    }
                }
            });
    }
    
    $scope.showDialog = function(id,user_id){
        $scope.current_dialog = id;
        $scope.current_user = user_id;    
        $http.get("?user2="+id).success(function(data){
            $scope.messages = data;
            setTimeout("jQuery('.scrollable-content').scrollTop(1024*1024);",100);
        });
    }
    
    $scope.newMessages = function(){
        if($scope.current_user == 0) return;    
        $http.get("?user2n="+$scope.current_dialog).success(function(data){ 
            for(var i=0;i<data.length;i++){
                var found = false;
                for(var j=0;j<$scope.dialogs.length;j++){
                    if($scope.messages[j]['id']==data[i]['id']){
                        found = true;
                        $scope.messages[j]['dt']==data[i]['dt'];
                        break;
                    }
                }                    
                if(!found){
                    $scope.messages.push(data[i]);
                    setTimeout("jQuery('.scrollable-content').scrollTop(1024*1024);",100);
                }
            }
        });        
    };
    
    $scope.sendMessage = function(){
        if($scope.current_user == 0) return false;    
        $http.post("",{'user':$scope.current_user,'text':$scope.current_message}).success(function(data){
            if(data!="") $scope.messages.push(data);        
            setTimeout("jQuery('.scrollable-content').scrollTop(1024*1024);",100);
        });   
        $scope.current_message = "";
        jQuery('.message-text').focus();
        return false;
    }
    
    $interval(function(){
        $scope.newMessages();
    },2000);
    
    
});

