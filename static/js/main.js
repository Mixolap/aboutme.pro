
jQuery(function () {
    //BEGIN BACK TO TOP
    jQuery(window).scroll(function(){
        if (jQuery(this).scrollTop() < 200) {
            jQuery('#totop') .fadeOut();
        } else {
            jQuery('#totop') .fadeIn();
        }
    });
    jQuery('#totop').on('click', function(){
        jQuery('html, body').animate({scrollTop:0}, 'fast');
        return false;
    });
    //END BACK TO TOP
});

var app = angular.module("MainApp",['ngSanitize']);

app.controller('MainController', function($scope,$interval,$http){
    $scope.removed_alerts = []
    $scope.news = [];
    $scope.new_messages_count = 0;
    
    $scope.updateNews = function(){
        if(angular.element("#check_alerts").val()=="1"){
            var url = "/api/actions/new/";
            //if($scope.news.length == 0) url += "?all=1"
            url += "?all=1"
            $http.get(url).success(function(data){ 
                for(var i=0;i<data.length;i++){
                    var found = false;
                    for(var j=0;j<$scope.news.length;j++){
                        if($scope.news[j]['id']==data[i]['id']){
                            found = true;
                            $scope.news[j]['dt']==data[i]['dt'];
                            break;
                        }
                    }                    
                    if(!found){
			            var pos = $scope.removed_alerts.indexOf(data[i]['id']); // ищем в удалённых оповещениях
			            if(pos<0) $scope.news.unshift(data[i]);
                    }
                }
                //if($scope.news.length == 0){
                //    $scope.news = data;
                //}
                //else{
                //    for(var i=0;i<data.length;i++){
                //        $scope.news.unshift(data[i]);
                //    }
                //}
            });
        }
		if(angular.element("#check_new_messages").val()=="1"){
			var url = "/api/messages/new/";
			$http.get(url).success(function(data){ 
				$scope.new_messages_count = data['count'];
			});
		}
    };
    
    $interval(function(){
        $scope.updateNews();
    },5000);
    
    
    $scope.hideAlert = function(id){
        var elem = 0;
        for(var i=0;i<$scope.news.length;i++){
            if($scope.news[i]['id'] == id){
                elem = $scope.news.splice(i,1);
                break;
            }        
        }
        if(elem!=0){
	        $scope.removed_alerts.push(id);
            $http.get('/action/'+id+'/hide/');
        }
    };
    
    $scope.topics = []
    
    $scope.loadTopics = function(){
            $http.get("?start="+$scope.topics.length).success(function(data){
                for(var i=0;i<data.length;i++){
                    $scope.topics.push(data[i]);
                }

                if(data.length<30) angular.element(".show_topics").hide()
            
            });
            return false;
    };
    
    $scope.bookmarkTopic = function(id){
        $http.get("/group/mark/"+id).success(function(data){
            angular.element("#btopic"+id).hide();
            return false;
        });
        return false;
    };

    $scope.hideComment = function(id){
        $http.get("/comment/"+id+"/hide/").success(function(data){
            angular.element("#comment"+id).hide();
            return false;
        });
        return false;
    };
});



