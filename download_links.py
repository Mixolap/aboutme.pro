#-*- encoding: utf-8 -*-
import django,os,datetime,shutil
import random
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "psotial.settings")
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
from psotial.models import *
from django.conf import settings


def downloadTopic(gt):
    if gt.link=="": return
    print "link=",gt.link
    PATH = "/opt/read"    
    id = str(gt.id)
    if os.path.exists(os.path.join(PATH,id)): return
    os.system("wget -E -H -k -K -p %s --directory-prefix=%s/%s -erobots=off --user-agent='Mozilla/5.0 (Windows NT 10.0; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0'"%(gt.link.split("&")[0],PATH,id))

if __name__=="__main__":
    print "downloading topics"
    topics = list(GroupTopic.objects.filter(link__isnull=False).exclude(link='').order_by("-id")[:690])
    random.shuffle(topics)
    for gt in topics :        
        downloadTopic(gt)
