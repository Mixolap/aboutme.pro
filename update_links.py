#-*- encoding: utf-8 -*-
import django,os,subprocess,datetime,shutil
import re
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "psotial.settings")
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from psotial.models import *

from django.conf import settings
try:
    from PIL import Image
except:
    import Image

PREFIX = '/opt/psotial/static/topics'
if sys.platform=='win32':    PREFIX = 'x:/home/mixolap/read'
def findHabrLink(id):
    filename = ""
    if os.path.exists(PREFIX+'/habr/%s/habrahabr.ru/post/%s/index.html?utm_source=habrahabr.html'%(id,id)):        
        shutil.move(PREFIX+'/habr/%s/habrahabr.ru/post/%s/index.html?utm_source=habrahabr.html'%(id,id),PREFIX+'/habr/%s/habrahabr.ru/post/%s/index.html'%(id,id))
    return '/static/topics/habr/%s/habrahabr.ru/post/%s/index.html'%(id,id)
    
def findYapLink(id):
    for d in xrange(50):
        if os.path.exists(PREFIX+'/yap/%s/www.yaplakal.com/forum%d/%s'%(id,d,id)):
            return '/static/topics/yap/%s/www.yaplakal.com/forum%d/%s'%(id,d,id)
        



for d in GroupTopic.objects.filter(xlink=None).order_by("-id"):
    if d.group_id==2:
        d.xlink = findHabrLink(d.extra)
        print d.xlink
        d.save()
        
    if d.group_id==3:
        d.xlink = findYapLink(d.extra)
        print d.xlink
        d.save()
        
        
        
    #print d.group_id,d.extra

exit()

def findFile(name,path):
    if not os.path.exists(path): return None
    for fname in os.listdir(path):
        fpath = os.path.join(path,fname)
        #print name in fpath
        if os.path.isdir(fpath):
            xpath = findFile(name,fpath)
            if xpath!=None: return xpath
            
        if name in fname:
            xpath = fpath
            return xpath
            
    return None
        


    


PATH = '/home/mixolap/read/topics'

def getHabrLink(gt):
    fname  = findFile('index.html?utm_source',os.path.join(PATH,str(gt.id)))
    fname2 = fname.split("?")[0]
    shutil.move(fname,fname2)
    
    return '/static/topics/topics'+fname2.replace(PATH,"")
    
def getYapLink(gt):
    fname  = findFile(gt.link.split('/')[-1],os.path.join(PATH,str(gt.id)))
    fname = fname.replace(".orig","")
    return '/static/topics/topics'+fname.replace(PATH,"")


def getRusellerLink(gt):
    fname = findFile("lessons.php.html",os.path.join(PATH,str(gt.id)))
    return '/static/topics/topics'+fname.replace(PATH,"")

def getLink(name,gt):
    fname = findFile(name,os.path.join(PATH,str(gt.id)))
    if fname==None: return None
    return '/static/topics/topics'+fname.replace(PATH,"")

for d in GroupTopic.objects.filter(xlink=None).exclude(link=None).order_by("-id"):
    print d.id,d.link,d.group.name
    if d.group.name == "Habrahabr":
        d.xlink = getHabrLink(d)
        print d.xlink
        d.save()
    elif d.group.name == "yaplakal.com":
        d.xlink = getYapLink(d)
        print d.xlink
        d.save()
    elif d.group.name == "ruseller":
        d.xlink = getRusellerLink(d)
        print d.xlink
        d.save()
    elif d.group.name == "css-live.ru":
        d.xlink = getLink(d.link.split("/")[-1],d)
        print d.xlink
        d.save()
    elif d.group.name == "mkdev.me":    
        d.xlink = getLink(d.link.split("/")[-1]+".html",d)
        print d.xlink
        d.save()
    elif d.group.name == "codehint.ru":
        d.xlink = getLink(d.link.split("/")[-1]+".html",d)
        print d.xlink
        d.save()
    elif d.group.name == "internet-technologies.ru":
        d.xlink = getLink(d.link.split("/")[-1],d)
        print d.xlink
        d.save()
    elif d.group.name == "lexev.org":
        d.xlink = getLink("index.html",d)
        print d.xlink
        d.save()        
    elif d.group.name == "Acman":
        d.xlink = getLink("index.html",d)
        print d.xlink
        d.save()                
    else:
        break

    

