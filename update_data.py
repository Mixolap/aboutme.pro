#-*- encoding: utf-8 -*-
import django,os,subprocess,datetime,shutil
import re
import feedparser
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "psotial.settings")
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from psotial.models import *
from psotial.funcs import *
from django.conf import settings

def processTopics():
    for gt in GroupTopic.objects.filter(is_processed=False,is_active=True).order_by("id"):
        for m in Man.objects.filter(is_active=True):
            if isInterestingPost(m,gt):
                createAction(m,gt)
        gt.is_processed = True
        gt.save()
        #break
            

def remove_html_tags(data):
    p = re.compile(r'<.*?>')
    return p.sub('', data)

def downloadRss(group):
    fd = feedparser.parse(group.src_link)    
    print(group.src_link,len(fd.entries))
    for d in fd.entries:
        if GroupTopic.objects.filter(group=group,title=d.title).count()>0: continue
        gt = GroupTopic(group=group)
        gt.author_id = 1
        gt.title = d.title
        gt.text  = remove_html_tags(d.description)
        gt.link  = d.link
        gt.xlink = d.link
        gt.save()

def downloadTopics():
    for d in Group.active.filter(src_link__isnull=False):
        downloadRss(d)

def updateTopicLinks():
    for d in Action.objects.filter(topic__isnull=True).order_by("-id"):        
        if GroupTopic.objects.filter(pk=d.link.split("/a")[1]).count()==0: continue
        #print d.link
        d.topic = GroupTopic.objects.get(pk=d.link.split("/a")[1])
        d.save()


def writeLink(f,link,dt):
    st="""
    <url>
        <loc>http://aboutme.pro%s</loc>
        <lastmod>%s</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.8</priority>
    </url>    
    """%(link,dt.strftime("%Y-%m-%d"))
    f.write(st)

def updateSitemap():
    fpath = '/opt/psotial/static/sitemap.xml'
    #if not os.path.exists(fpath): return
    #from fforum.models import Theme
    fw = file(fpath,"w")
    fw.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    fw.write('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">')
    
    for d in Man.objects.filter(is_active=True):
        writeLink(fw,d.get_absolute_url(),d.user.date_joined)
        
    for d in GroupTopic.objects.filter(group=None,is_active=True):
        writeLink(fw,d.get_absolute_url(),d.dt)
        
    fw.write('</urlset>')
    fw.close()
    
    
    
if __name__=="__main__":
    #updateTopicLinks()
    #exit()
    print "downloading topics"
    downloadTopics()
    print "processing topics"
    processTopics()
    print "updating sitemap"
    updateSitemap()
    
    


    

